﻿using System;

namespace vowels_number
{
    class VowelsEnum
    {
        // enumeration for vowels
        enum Vowels
        {
            a, e, i, o, u
        };
        static void Main(string[] args)
        {
            // get input as string
            Console.WriteLine("Enter string");
            String word = Console.ReadLine(); 

            // if user press enter key without input
            while (word == "")
            {
                Console.WriteLine("Null values not accepted ! Enter a word");
                word = Console.ReadLine();
            }

            // print vowels number and consonents
            for (int index = 0; index < word.Length; index++) {
                switch(word[index])
                {
                    case  'a':
                    case 'A':
                        Console.WriteLine(word[index] + " - Vowel Number " + (int)Vowels.a);
                        break;
                    case 'e':
                    case 'E':
                        Console.WriteLine(word[index] + " - Vowel Number " + (int)Vowels.e);
                        break;
                    case 'i':
                    case 'I':
                        Console.WriteLine(word[index] + " - Vowel Number " + (int)Vowels.i);
                        break;
                    case 'o':
                    case 'O':
                        Console.WriteLine(word[index] + " - Vowel Number " + (int)Vowels.o);
                        break;
                    case 'u':
                    case 'U':
                        Console.WriteLine(word[index]+ " - Vowel Number " + (int)Vowels.u);
                        break;
                    default :
                        Console.WriteLine($"{word[index]} Consonent");
                        break;
                }
            }
        }
    }
}
